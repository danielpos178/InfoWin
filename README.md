# InfoEasy

> Un mod mai simplu si mai eficient in care poti invata informatica pentru bac

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-purple.svg)](https://www.gnu.org/licenses/gpl-3.0)

<p align="left">
  <a href="https://skillicons.dev">
    <img src="https://skillicons.dev/icons?i=react,tailwindcss,html,css,js" alt=""/>
  </a>
</p>


### **Continutul lectiilor a fost scris pe baza informatilor de pe pbinfo.ro si infoas.ro**

## Screenshots
 <img title="speed" alt="speed data" src="./src/assets/ss2.png">
 <img title="files" alt="file struct" src="./src/assets/ss1.png">
 <img title="speed" alt="speed data" src="./src/assets/speed-ss.png">
 <img title="files" alt="file struct" src="./src/assets/filestructure-ss.png">
 <img title="files" alt="file struct" src="./src/assets/vscode-ss.png">
 <img title="files" alt="file struct" src="./src/assets/tailwindcss-ss.png">






## Licenta

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-purple.svg)](https://www.gnu.org/licenses/gpl-3.0)
[Licese text](License.md)

        This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.

        This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/
