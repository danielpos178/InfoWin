import './index.css'
import { Route, RouterProvider, createBrowserRouter, createRoutesFromElements } from 'react-router-dom'
import Layout from './Layout.jsx'
import Home from "./pages/Home"
import Operatori from "./pages/Operatori/Operatori.jsx"
import Aritmetici from './pages/Operatori/Aritmetici.jsx'
import Testaritm from './pages/Operatori/Testaritm.jsx'
import Atribuire from './pages/Operatori/Atribuire.jsx'
import Testartrib from "./pages/Operatori/Testartrib.jsx";
import { useEffect } from "react"
import Relationali from './pages/Operatori/Relationali.jsx'
import Testrel from './pages/Operatori/Testrel.jsx'
import Logici from "./pages/Operatori/Logici.jsx";
import Testlogici from "./pages/Operatori/Testlogici.jsx";
import Subiecte from "./pages/SubiecteBac/Subiecte.jsx";
import S24 from "./pages/SubiecteBac/S24.jsx";
import S23 from "./pages/SubiecteBac/S23.jsx";
import S22 from "./pages/SubiecteBac/S22.jsx";
import S21 from "./pages/SubiecteBac/S21.jsx";
import S20 from "./pages/SubiecteBac/S20.jsx";
import S19 from "./pages/SubiecteBac/S19.jsx";
import S18 from "./pages/SubiecteBac/S18.jsx";
import S17 from "./pages/SubiecteBac/S17.jsx";
import S16 from "./pages/SubiecteBac/S16.jsx";
import S15 from "./pages/SubiecteBac/S15.jsx";
import S14 from "./pages/SubiecteBac/S14.jsx";
import S13 from "./pages/SubiecteBac/S13.jsx";
import S12 from "./pages/SubiecteBac/S12.jsx";
import S11 from "./pages/SubiecteBac/S11.jsx";
import S10 from "./pages/SubiecteBac/S10.jsx";
import Docs from './pages/Documentatie.jsx'
import PE from './pages/Error.jsx'
import CPPPage from './pages/Cpp.jsx'
import Pseudocod from './pages/Pseudocod.jsx'
import Pseudocodtest from './pages/Pseudocodtest.jsx'

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path='/' element={<Layout />}>
      <Route path='' element={<Home />}/>
      <Route path='app/operatori' element={<Operatori />} />
        <Route path='app/operatori/aritmetici' element={<Aritmetici/>} />
        <Route path='app/operatori/aritmetici/test' element={<Testaritm/>}/>
        <Route path='app/operatori/atribuire' element={<Atribuire/>} />
        <Route path='app/operatori/atribuire/test' element={<Testartrib/>} />
        <Route path='app/operatori/relationali' element={<Relationali/>} />
        <Route path='app/operatori/relationali/test' element={<Testrel/>} />
        <Route path='app/operatori/logici' element={<Logici/>} />
        <Route path='app/operatori/logici/test' element={<Testlogici/>} />
        <Route path='/*' element={<PE/>} />
        <Route path='app/bac' element={<Subiecte/>} />
        <Route path='app/bac/s24' element={<S24/>} />
        <Route path='app/bac/s23' element={<S23/>} />
        <Route path='app/bac/s22' element={<S22/>} />
        <Route path='app/bac/s21' element={<S21/>} />
        <Route path='app/bac/s20' element={<S20/>} />
        <Route path='app/bac/s19' element={<S19/>} />
        <Route path='app/bac/s18' element={<S18/>} />
        <Route path='app/bac/s17' element={<S17/>} />
        <Route path='app/bac/s16' element={<S16/>} />
        <Route path='app/bac/s15' element={<S15/>} />
        <Route path='app/bac/s14' element={<S14/>} />
        <Route path='app/bac/s13' element={<S13/>} />
        <Route path='app/bac/s12' element={<S12/>} />
        <Route path='app/bac/s11' element={<S11/>} />
        <Route path='app/bac/s10' element={<S10/>} />
        <Route path='docs' element={<Docs/>} />
        <Route path='app/cpp' element={<CPPPage/>} />
        <Route path='app/pseudocod' element={<Pseudocod/>} />
        <Route path='app/pseudocod/test' element={<Pseudocodtest/>} />


    </Route>
  )
)

function App() {
  const googleTranslateElementInit = () => {
    new window.google.translate.TranslateElement(
      {
        pageLanguage: "ro",
        autoDisplay: false
      },
      "google_translate_element"
    );
  };
  useEffect(() => {
    let addScript = document.createElement("script");
    addScript.setAttribute(
      "src",
      "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"
    );
    document.body.appendChild(addScript);
    window.googleTranslateElementInit = googleTranslateElementInit;
  }, []);

return (
    <>
      <RouterProvider router={router} />
      <div className='flex flex-row-reverse bg-white dark:bg-black'>
        <div id="google_translate_element"></div>
      </div>
    </>
  );
}

export default App;