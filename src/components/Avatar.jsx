import { Menu, Transition } from '@headlessui/react'
import { Fragment } from 'react'
import LoginButton from './Login'
import LogoutButton from './Logout'
import ThemeSwitch from "./ThemeSwitch.jsx"
import Mico from "../assets/menuico.jsx"

export default function Example() {
  return (
    <div className="top-16 text-left">
      <Menu as="div" className="relative inline-block text-left">
        <div>
          <Menu.Button className="inline-flex w-full justify-center rounded-md bg-darkred px-0 py-2 text-sm font-medium text-white hover:bg-red focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75">
            <Mico/>
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className=" right-0 absolute mt-2 w-32 origin-top-right divide-y divide-white rounded-md bg-darkred shadow-lg ring-1 ring-darkred/5 focus:outline-none">
            <div className="px-1 py-2 ">
              <Menu.Item>
                {({ active }) => (
                  <button
                    className={`${
                      active ? 'bg-red text-white' : 'text-white'
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                    
                    >
                    <LoginButton/>
                  </button>
                )}
              </Menu.Item>
                    <Menu.Item>
                        {({ active }) => (
                        <button
                            className={`${
                                active ? 'bg-red text-white' : 'text-white'
                            } group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                            <LogoutButton/>
                        </button>
                        )}
                    </Menu.Item>
                    <Menu.Item>
                        {({ active }) => (
                        <button
                            className={`${
                                active ? 'bg-red text-white' : 'text-white'
                            } group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                            <ThemeSwitch/>
                        </button>
                        )}
                    </Menu.Item>
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
    
  )
}