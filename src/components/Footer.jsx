import {Logo} from "./Logo.jsx";

function App() {

    return (
        <>
            <footer className="text-black dark:text-white bg-white dark:bg-black body-font">
                <div className="flex justify-center">
                    <hr className="w-2/3 align-middle mt-1"/>
                </div>
                <div className="container px-5 py-8 mx-auto flex items-center sm:flex-row flex-col">
                    <a className="flex title-font font-medium items-center md:justify-start justify-center text-white">
                        <Logo color="#933333" />
                    </a>
                    <p className=" text-black dark:text-white sm:ml-4 sm:pl-4 sm:border-l-2 sm:border-black sm:py-2 sm:mt-0 mt-4">An open source project made with <span className="text-red-600 text-lg">&#9829;</span> and React by
                        <a href="https://gitlab.com/danielpos178" className="text-gray-500 ml-1" target="_blank" rel="noopener noreferrer">@danielpos178</a>
                    </p>
                    <span className="inline-flex sm:ml-auto sm:mt-0 mt-4 justify-center sm:justify-start">
                        <a className="text-black dark:text-white" href="https://gitlab.com/danielpos178">
                            <svg fill="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
                                <path d="M4.845.904c-.435 0-.82.28-.955.692C2.639 5.449 1.246 9.728.07 13.335a1.437 1.437 0 0 0 .522 1.607l11.071 8.045c.2.145.472.144.67-.004l11.073-8.04a1.436 1.436 0 0 0 .522-1.61c-1.285-3.942-2.683-8.256-3.817-11.746a1.004 1.004 0 0 0-.957-.684.987.987 0 0 0-.949.69L15.8 9.001H8.203l-2.41-7.408a.987.987 0 0 0-.942-.69h-.006zm-.006 1.42 2.173 6.678H2.675zm14.326 0 2.168 6.678h-4.341zm-10.593 7.81h6.862c-1.142 3.52-2.288 7.04-3.434 10.559L8.572 10.135zm-5.514.005h4.321l3.086 9.5zm13.567 0h4.325c-2.467 3.17-4.95 6.328-7.411 9.502 1.028-3.167 2.059-6.334 3.086-9.502zM2.1 10.762l6.977 8.947-7.817-5.682a.305.305 0 0 1-.112-.341zm19.798 0 .952 2.922a.305.305 0 0 1-.11.341v.002l-7.82 5.68.026-.035z"></path>
                            </svg>
                        </a>
                        <a className="ml-3 text-black dark:text-white" href="https://github.com/Daniel1788">
                            <svg fill="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
                                <path d="M 12 0 C 5.371094 0 0 5.371094 0 12 C 0 17.308594 3.433594 21.796875 8.203125 23.386719 C 8.804688 23.488281 9.03125 23.128906 9.03125 22.816406 C 9.03125 22.53125 9.015625 21.585938 9.015625 20.578125 C 6 21.136719 5.21875 19.84375 4.980469 19.171875 C 4.84375 18.824219 4.261719 17.761719 3.75 17.476562 C 3.328125 17.25 2.730469 16.695312 3.734375 16.679688 C 4.679688 16.664062 5.355469 17.550781 5.578125 17.910156 C 6.660156 19.726562 8.386719 19.214844 9.074219 18.898438 C 9.179688 18.121094 9.496094 17.59375 9.839844 17.296875 C 7.171875 16.996094 4.378906 15.960938 4.378906 11.371094 C 4.378906 10.066406 4.84375 8.984375 5.609375 8.144531 C 5.488281 7.84375 5.070312 6.613281 5.730469 4.964844 C 5.730469 4.964844 6.734375 4.648438 9.03125 6.195312 C 9.988281 5.925781 11.011719 5.789062 12.03125 5.789062 C 13.050781 5.789062 14.070312 5.925781 15.03125 6.195312 C 17.324219 4.636719 18.328125 4.964844 18.328125 4.964844 C 18.988281 6.613281 18.570312 7.84375 18.449219 8.144531 C 19.214844 8.984375 19.679688 10.050781 19.679688 11.371094 C 19.679688 15.976562 16.875 16.996094 14.203125 17.296875 C 14.640625 17.671875 15.015625 18.390625 15.015625 19.515625 C 15.015625 21.121094 15 22.410156 15 22.816406 C 15 23.128906 15.226562 23.503906 15.824219 23.386719 C 20.566406 21.796875 24 17.296875 24 12 C 24 5.371094 18.628906 0 12 0 Z M 12 0 "/>
                            </svg>
                        </a>
                        <a className="ml-3 text-black dark:text-white" href="https://www.instagram.com/d.__.aniel/">
                            <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
                                <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
                                <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
                            </svg>
                        </a>
                    </span>
                </div>
            </footer>
        </>
      );
    }
    
    export default App;