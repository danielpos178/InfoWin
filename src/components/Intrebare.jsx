/* eslint-disable react/prop-types */
import { useState } from "react";

const Intrebare = ({ intrebare, raspunsCorect, explicatie, onRaspunsCorect }) => {
  const [raspuns, setRaspuns] = useState("");
  const [showExplicatie, setShowExplicatie] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (raspuns.trim().toLowerCase() === raspunsCorect.toLowerCase()) {
      onRaspunsCorect();
      setRaspuns("");
      setShowExplicatie(false);
    } else {
      setShowExplicatie(true);
    }
  };

  return (
    <div className="mb-4">
      <p className="text-lg font-semibold">{intrebare}</p>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          className="border border-gray-400 px-2 py-1"
          value={raspuns}
          onChange={(e) => setRaspuns(e.target.value)}
        />
        <button
          type="submit"
          className="ml-2 bg-blue-500 text-white px-3 py-1 rounded hover:bg-blue-600"
        >
          Trimite Raspuns
        </button>
      </form>
      {showExplicatie && <p className="text-red-600">{explicatie}</p>}
    </div>
  );
};

export default Intrebare;
