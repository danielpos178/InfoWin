import Avatar from "./Avatar.jsx";
import {Logo} from "./Logo.jsx";
import Navmenu from "./Navmenu.jsx";

export default function Navbar() {

  return (
    <>
        <div className="bg-darkred w-full flex justify-evenly h-14 content-center pt-2">
        <div>
        <Navmenu ></Navmenu>
        </div>
        <a href="/"><Logo color="white"></Logo></a>
        <Avatar/>
        </div>
    </>
  );
}