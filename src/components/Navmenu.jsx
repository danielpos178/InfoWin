import { Menu, Transition } from '@headlessui/react'
import { Fragment } from 'react'
import { ChevronDownIcon } from '@heroicons/react/20/solid'

export default function Example() {
  return (
    <div className="top-16 text-left">
      <Menu as="div" className="relative inline-block text-left">
        <div>
          <Menu.Button className="inline-flex w-full justify-center rounded-md bg-darkred px-4 py-2 text-sm font-medium text-white hover:bg-red focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75">
            Pagini
            <ChevronDownIcon
              className="-mr-1 ml-1 h-5 w-5 text-white hover:text-white"
              aria-hidden="true"
            />
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className=" -right-40 absolute mt-2 w-56 origin-top-right divide-y divide-white rounded-md bg-darkred shadow-lg ring-1 ring-darkred/5 focus:outline-none">
            <div className="px-1 py-1 ">
              <Menu.Item>
                {({ active }) => (
                    <a href='/app/operatori'>
                      <button
                          className={`${
                              active ? 'bg-red text-white' : 'text-white'
                          } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                      >
                        Operatori si expresii
                      </button>
                    </a>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <a href='/app/pseudocod'>
                  <button
                    className={`${
                        active ? 'bg-red text-white' : 'text-white'
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                    Pseudocod
                  </button>
                  </a>
                )}
              </Menu.Item>
            </div>
            <div className="px-1 py-1">
              <Menu.Item>
                {({ active }) => (
                  <a href='/app/cpp'>
                  <button
                    className={`${
                        active ? 'bg-red text-white' : 'text-white'
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                    C++
                  </button>
                  </a>
                )}
              </Menu.Item>

            </div>
            <div className="px-1 py-1">
              <Menu.Item>
                {({ active }) => (
                    <a href="/app/bac">
                      <button
                          className={`${
                              active ? 'bg-red text-white' : 'text-white'
                          } group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                        Subiecte Bacalaureat
                      </button>
                    </a>
                )}
              </Menu.Item>
            </div>
            <div className="px-1 py-1">
            <Menu.Item>
                {({ active }) => (
                  <a href='/docs'>
                  <button
                    className={`${
                        active ? 'bg-red text-white' : 'text-white'
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                    Documentatie
                  </button>
                  </a>
                )}
              </Menu.Item>
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
    
  )
}