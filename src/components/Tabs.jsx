import { Tab } from '@headlessui/react'
import AccountTreeOutlinedIcon from '@mui/icons-material/AccountTreeOutlined';
import QuizOutlinedIcon from '@mui/icons-material/QuizOutlined';
import DonutSmallOutlinedIcon from '@mui/icons-material/DonutSmallOutlined';

export default function Tabs() {
    return (
        <div className='bg-darkred text-white w-screen h-full min-h-96 flex flex-col justify-start content-center pt-2'>
            <Tab.Group defaultIndex={1}>
                <Tab.List className='pb-10 flex flex-wrap flex-row justify-center content-center' >
                    <Tab className="p-5 hover:bg-red rounded-md outline-none focus:border-2 text-xl">Despre</Tab>
                    <Tab className="p-5 hover:bg-red rounded-md outline-none focus:border-2 text-xl">Ce oferim</Tab>
                    <Tab className="p-5 hover:bg-red rounded-md outline-none focus:border-2 text-xl">Cine suntem</Tab>
                </Tab.List>
                <Tab.Panels className='flex flex-wrap flex-row justify-center content-center '>
                        <Tab.Panel>
                            <div className='lg:pl-48 lg:pr-48  pr-10 pl-10 pb-10'>
                                <p className='text-justify text-xl sm:text-lg'>Bun venit pe InfoEasy, platforma educațională care îți face învățarea informaticii mai ușoară și mai accesibilă că niciodată! Indiferent dacă eșți la început de drum sau vrei să îți consolidezi cunoștințele de pseudocod sau C++, InfoEasy îți oferă resurse interactive și lecții bine structurate pentru a-ți atinge obiectivele educaționale.</p>
                                <p className='text-center text-2xl sm:text-xl pt-10'>Începe astăzi aventura ta în lumea fascinantă a informaticii cu InfoEasy și descoperă cât de ușor poate fi să înveți și să te dezvolți în acest domeniu atractiv!</p>
                            </div>
                        </Tab.Panel>
                        <Tab.Panel>
                            <section className="text-white body-font">	
                                <div className="container px-5 pt-7 pb-24 mx-auto">
                                    <h1 className="sm:text-3xl text-2xl font-medium title-font text-center text-white mb-20">Platforma educationala
                                        <br className="hidden sm:block"/> <span className='text-lightred'>InfoEasy</span> ofera
                                    </h1>
                                    <div className="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4 md:space-y-0 space-y-6">
                                        <div className="p-4 md:w-1/3 flex">
                                            <div className="w-14 h-14 inline-flex items-center justify-center rounded-full mb-4 flex-shrink-0">
                                                <AccountTreeOutlinedIcon/>
                                            </div>
                                            <div className="flex-grow pl-6">
                                                <h2 className="text-white text-lg title-font font-medium mb-2">Lecții bine structurate și organizate în funcție de dificultate</h2>
                                                <p className="leading-relaxed text-base">Începe cu lecții structurate pentru a înțelege bazele pseudocodului și ale limbajului de programare C++. Lecțiile noastre sunt concepute pentru a eficientiza procesul de învățare. Acestea sunt ordonate în funcție de dificultate și fiecăreia îi este atașată o resura externă</p>
                                            </div>
                                        </div>
                                        <div className="p-4 md:w-1/3 flex">
                                            <div className="w-12 h-12 inline-flex items-center justify-center rounded-full mb-4 flex-shrink-0">
                                                <QuizOutlinedIcon/>
                                            </div>
                                            <div className="flex-grow pl-6">
                                                <h2 className="text-white text-lg title-font font-medium mb-2">Exerciții cu ajutorul cărora îți poți evalua cunoștințele dobândite</h2>
                                                <p className="leading-relaxed text-base">Sistemul nostru de exerciții și întrebări se bazează pe aprofundarea cunoștințelor oferindu-ți și un mod distractiv de a-ți îmbunătății abilitățile practice.</p>
                                            </div>
                                        </div>
                                        <div className="p-4 md:w-1/3 flex">
                                            <div className="w-12 h-12 inline-flex items-center justify-center rounded-full mb-4 flex-shrink-0">
                                                <DonutSmallOutlinedIcon/>
                                            </div>
                                            <div className="flex-grow pl-6">
                                                <h2 className="text-white text-lg title-font font-medium mb-2">Posibilitatea de a-ți urmării progresul</h2>
                                                <p className="leading-relaxed text-base">Prin intermediul funcționalităților noastre de monitorizare a progresului, vei ști mereu unde te afli în călătoria ta educativă. De asemenea, pentru fiecare lecție parcursă, vei fi recompensat.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </Tab.Panel>
                    <Tab.Panel>
                        <section className="body-font">
                            <div className="container pl-5 pr-5 pb-28 flex flex-col">
                                <div className="lg:w-4/6 mx-auto">
                                    <div className="flex flex-col sm:flex-row mt-10">
                                        <div className="sm:w-1/3 text-center sm:pr-8 sm:py-8">
                                            <div
                                                className="w-20 h-20 rounded-full inline-flex items-center justify-center bg-gray-800 text-gray-600">
                                                <svg fill="none" stroke="currentColor" stroke-linecap="round"
                                                     stroke-linejoin="round" stroke-width="2" className="w-10 h-10"
                                                     viewBox="0 0 24 24">
                                                    <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                                                    <circle cx="12" cy="7" r="4"></circle>
                                                </svg>
                                            </div>
                                            <div className="flex flex-col items-center text-center justify-center">
                                                <h2 className="font-medium title-font mt-4 text-white text-lg">Postelnicu Ioachim-Florian-Daniel</h2>
                                                <div className="w-12 h-1 bg-blue-400 rounded mt-2 mb-4"></div>

                                            </div>
                                        </div>
                                        <div
                                            className="sm:w-2/3 sm:pl-8 sm:py-8 sm:border-l border-blue-400 sm:border-t-0 border-t mt-4 pt-4 sm:mt-0 text-center sm:text-left">
                                            <p className="leading-relaxed text-lg mb-5 pt-1 md:pt-8">InfoEasy este o platforma educațională dezvoltată pentru Olimpiada de inovare și creație digitală - InfoEducatie 2024 și
                                                a fost concepută pentru a aduce o experiență educațională inovatoare și captivantă în casele și școlile din întreagă lume.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </Tab.Panel>
                </Tab.Panels>
            </Tab.Group>
        </div>

    )
}