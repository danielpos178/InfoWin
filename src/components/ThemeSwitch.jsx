// DarkLightModeSwitch.js
import { useState } from 'react';
import { FiMoon, FiSun } from 'react-icons/fi';

import '../index.css'

const DarkLightModeSwitch = () => {
  const storedDarkMode = localStorage.getItem('darkMode');
  const initialDarkMode = storedDarkMode === 'true' || storedDarkMode === null ? true : false;
  const [darkMode, setDarkMode] = useState(initialDarkMode);

  const handleModeChange = () => {
    const newMode = !darkMode;
    setDarkMode(newMode);
    localStorage.setItem('darkMode', newMode);
    document.documentElement.classList.toggle('dark', newMode);
  };

  return (
    <div className="flex items-center justify-center mt-1">
      <label htmlFor="darkMode" className="flex items-center cursor-pointer">
        <span className="mr-3">Change theme</span>
        <div className="relative">
          <input
            type="checkbox"
            id="darkMode"
            className="hidden"
            checked={darkMode}
            onChange={handleModeChange}
          />
          <div className="toggle__line w-10 h-4 bg-gray-400 rounded-full shadow-inner"></div>
          <div className={`moon-sun ${darkMode ? 'moon' : 'sun'}`}>
            {darkMode ? <FiMoon /> : <FiSun />}
          </div>
        </div>
      </label>
    </div>
  );
};

export default DarkLightModeSwitch;
