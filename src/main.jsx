import React from 'react';
import { createRoot } from 'react-dom/client';
import { Auth0Provider } from '@auth0/auth0-react';
import App from './App';
import './index.css';
import { SpeedInsights } from "@vercel/speed-insights/react"


const root = createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <Auth0Provider
      domain="dev-kfcf8jp2rj8leikr.eu.auth0.com"
      clientId="V3g1X9TOmewst3ODsUpzn4sct8vZVvnJ"
      authorizationParams={{
        redirect_uri: window.location.origin
      }}
    >
    <div className='w-screen bg-white dark:bg-black h-screen'>
      <App />
    </div>
    </Auth0Provider>
    <SpeedInsights/>
  </React.StrictMode>
);