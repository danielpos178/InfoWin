function CPPPage() {
    return (
      <div className="min-h-screen bg-white dark:bg-black text-darkblue dark:text-blue-300">
        <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
          <div className="text-center">
            <h2 className="text-lg leading-6 font-semibold text-darkblue dark:text-blue-300 uppercase tracking-wider">Clasele 9-11</h2>
            <p className="mt-2 text-3xl font-extrabold text-darkblue dark:text-blue-300 sm:text-4xl">
              C++
            </p>
            <p className="mt-2 text-lg text-gray-600 dark:text-gray-300">
              Limbaj de programare
            </p>
          </div>
  
          <div className="mt-10 text-darkblue dark:text-blue-300">
            <div className="grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-3">
              {/* Section 1: Introduction */}
              <div className="bg-blue-50 dark:bg-gray-800 rounded-lg shadow divide-y divide-gray-200 dark:divide-gray-700">
                <div className="p-6">
                  <h3 className="text-lg font-semibold text-blue-600 dark:text-blue-300 underline">Introducere în C++</h3>
                  <p className="mt-2 text-base text-gray-600 dark:text-gray-300">C++ este un limbaj de programare puternic și versatil, folosit pentru dezvoltarea de aplicații software de diverse tipuri.</p>
                </div>
              </div>
  
              {/* Section 2: Syntax and Basics */}
              <div className="bg-blue-50 dark:bg-gray-800 rounded-lg shadow divide-y divide-gray-200 dark:divide-gray-700">
                <div className="p-6">
                  <h3 className="text-lg font-semibold text-blue-600 dark:text-blue-300 underline">Sintaxă și Concepte de Bază</h3>
                  <p className="mt-2 text-base text-gray-600 dark:text-gray-300">C++ folosește o sintaxă similară cu C, dar adaugă concepte de programare orientată pe obiecte.</p>
                </div>
              </div>
  
              {/* Section 3: Advanced Topics */}
              <div className="bg-blue-50 dark:bg-gray-800 rounded-lg shadow divide-y divide-gray-200 dark:divide-gray-700">
                <div className="p-6">
                  <h3 className="text-lg font-semibold text-blue-600 dark:text-blue-300 underline">Teme Avansate</h3>
                  <p className="mt-2 text-base text-gray-600 dark:text-gray-300">C++ permite lucrul cu pointeri, gestionarea memoriei și alte concepte avansate.</p>
                </div>
              </div>
            </div>
          </div>
  
          {/* Resources Section */}
          <div className="mt-12 text-center">
            <h3 className="text-lg leading-6 font-semibold text-blue-600 dark:text-blue-500 uppercase tracking-wider">Resurse Suplimentare</h3>
            <div className="mt-2 flex justify-center">
              <a href="https://www.w3schools.com/cpp/default.asp" className="text-base font-medium text-blue-600 dark:text-blue-500 hover:text-blue-400">
                C++ Tutorial
              </a>
              <span className="mx-2 text-base text-gray-500 dark:text-gray-400">•</span>
              <a href="https://en.cppreference.com/w/" className="text-base font-medium text-blue-600 dark:text-blue-500 hover:text-blue-500">
                C++ Reference
              </a>
              <span className="mx-2 text-base text-gray-500 dark:text-gray-400">•</span>
              <a href="https://ro-static.z-dn.net/files/df6/908fe154366a46c397e82b9120877f1c.pdf" className="text-base font-medium text-blue-600 dark:text-blue-500 hover:text-blue-500">
                Carte: Programarea in limbajul C/C++ pentru liceu
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
  
  export default CPPPage;