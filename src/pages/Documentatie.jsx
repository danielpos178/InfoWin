import { useState } from 'react';
import { Dialog } from '@headlessui/react';

function Docs() {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  return (
    <>
      <div className="flex flex-col text-center w-full dark:text-blue-200 text-darkblue pt-10 sm:pt-16 pb-10 items-center">
        <h1 className="sm:text-5xl text-3xl title-font mb-1 font-bold">Documentație Tehnică</h1>
        <h2 className="text-md tracking-widest font-medium title-font mb-10">- InfoEasy -</h2>
      </div>
      <main className="flex flex-col md:flex-row h-screen dark:text-blue-200 text-darkblue bg-white dark:bg-black pt-10">
        <button 
          className="md:hidden p-4 text-darkblue dark:text-blue-200" 
          onClick={() => setSidebarOpen(true)}
        >
          Meniu
        </button>
        <Dialog 
          as="div"
          className="relative z-10 md:hidden"
          open={sidebarOpen}
          onClose={() => setSidebarOpen(false)}
        >
          <div className="fixed inset-0 bg-black bg-opacity-30" />
          <div className="fixed inset-0 flex items-center justify-center p-4">
            <Dialog.Panel className="w-full max-w-xs bg-white dark:bg-black p-6 rounded-lg shadow-lg">
              <Dialog.Title className="text-lg font-bold text-darkblue dark:text-blue-200">Meniu</Dialog.Title>
              <ul className="space-y-4 mt-4 text-blue-600 dark:text-blue-100">
                <li><a href="#overview" className="hover:text-blue-500 dark:hover:text-blue-300" onClick={() => setSidebarOpen(false)}>Prezentare generală</a></li>
                <li><a href="#tema" className="hover:text-blue-500 dark:hover:text-blue-300" onClick={() => setSidebarOpen(false)}>Alegerea temei</a></li>
                <li><a href="#installation" className="hover:text-blue-500 dark:hover:text-blue-300" onClick={() => setSidebarOpen(false)}>Instalare</a></li>
                <li><a href="#features" className="hover:text-blue-500 dark:hover:text-blue-300" onClick={() => setSidebarOpen(false)}>Caracteristici</a></li>
                <li><a href="#usage" className="hover:text-blue-500 dark:hover:text-blue-300" onClick={() => setSidebarOpen(false)}>Utilizare</a></li>
                <li><a href="#technologies" className="hover:text-blue-500 dark:hover:text-blue-300" onClick={() => setSidebarOpen(false)}>Tehnologii</a></li>
                <li><a href="#system-requirements" className="hover:text-blue-500 dark:hover:text-blue-300" onClick={() => setSidebarOpen(false)}>Cerințe de sistem</a></li>
                <li><a href="#useful-links" className="hover:text-blue-500 dark:hover:text-blue-300" onClick={() => setSidebarOpen(false)}>Linkuri utile</a></li>
                <li><a href="#contribution" className="hover:text-blue-500 dark:hover:text-blue-300" onClick={() => setSidebarOpen(false)}>Contribuție</a></li>
              </ul>
            </Dialog.Panel>
          </div>
        </Dialog>
        <div className="hidden md:block md:w-1/5 h-full max-h-screen overflow-y-auto bg-purple-50 dark:bg-gray-800">
          <div className="h-full flex items-center px-5">
            <div className="w-full h-5/6 bg-white dark:bg-gray-900 flex flex-row content-between">
              <div className="bg-darkblue p-5 min-w-80 flex flex-col text-center text-white">
                <h3 className="text-lg font-bold mb-4">Meniu</h3>
                <ul className="space-y-4">
                  <li><a href="#overview" className="hover:text-blue-300">Prezentare generală</a></li>
                  <li><a href="#tema" className="hover:text-blue-300">Alegerea temei</a></li>
                  <li><a href="#installation" className="hover:text-blue-300">Instalare locala</a></li>
                  <li><a href="#features" className="hover:text-blue-300">Caracteristici</a></li>
                  <li><a href="#usage" className="hover:text-blue-300">Utilizare</a></li>
                  <li><a href="#technologies" className="hover:text-blue-300">Tehnologii</a></li>
                  <li><a href="#system-requirements" className="hover:text-blue-300">Cerințe de sistem</a></li>
                  <li><a href="#useful-links" className="hover:text-blue-300">Linkuri utile</a></li>
                  <li><a href="#contribution" className="hover:text-blue-300">Contribuție</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="md:w-4/5 dark:text-blue-200 text-darkblue max-h-screen overflow-y-auto pr-6 sm:pr-14 text-justify pl-6 sm:pl-14">
          <section id="overview" className="mb-10">
            <h2 className="text-2xl font-bold mb-4">Prezentare generală</h2>
            <p>
              InfoEasy este o aplicație web educațională proiectată pentru a facilita învățarea și gestionarea informațiilor. Utilizând tehnologii moderne precum React și TailwindCSS, aplicația oferă o interfață intuitivă și plăcută utilizatorilor.
            </p>
          </section>
          <section id="tema" className="mb-10">
            <h2 className="text-2xl font-bold mb-4">Alegerea temei</h2>
            <p>
            Motivația mea pentru a alege să creez o platformă educațională de învățare a informaticii provine din convingerea că accesul la educație de calitate în acest domeniu este esențial pentru dezvoltarea personală și profesională a fiecărui individ în lumea modernă. Informatica nu este doar o disciplină academică, ci și o competență fundamentală pentru succesul în numeroase domenii, de la tehnologie la afaceri și știință.            
            </p>
            <p className='pt-2'>
            Prin crearea unei platforme educaționale dedicate învățării informaticii, intenționez să ofer oportunități egale de învățare pentru toți cei interesați, indiferent de vârstă, locație sau resurse financiare. Înțelegerea conceptelor de bază ale informaticii poate deschide uși către cariere promițătoare și poate stimula gândirea critică, rezolvarea problemelor și creativitatea.            
            </p>
          </section>
          <section id="installation" className="mb-10">
            <h2 className="text-2xl font-bold mb-4">Instalare</h2>
            <p>
              Urmați acești pași pentru a instala aplicația local:
            </p>
            <ol className="list-decimal ml-5">
              <li>Clonați repository-ul de pe GitHub:</li>
              <pre className="bg-gray-100 p-2 rounded">git clone https://gitlab.com/danielpos178/InfoEasy.git</pre>
              <li>Accesați directorul proiectului:</li>
              <pre className="bg-gray-100 p-2 rounded">cd InfoEasy</pre>
              <li>Instalați dependențele:</li>
              <pre className="bg-gray-100 p-2 rounded">npm install</pre>
              <li>Rulați serverul de dezvoltare:</li>
              <pre className="bg-gray-100 p-2 rounded">npm run dev</pre>
            </ol>
          </section>
          <section id="features" className="mb-10">
            <h2 className="text-2xl font-bold mb-4">Caracteristici</h2>
            <ul className="list-disc ml-5">
              <li>Interfață prietenoasă și intuitivă.</li>
              <li>Componente reutilizabile pentru dezvoltare rapidă.</li>
              <li>Stilizare flexibilă și modernă cu TailwindCSS.</li>
              <li>Funcționalități avansate pentru aprofundarea și verificarea cunoștințelor dobândite.</li>
            </ul>
          </section>
          <section id="usage" className="mb-10">
            <h2 className="text-2xl font-bold mb-4">Utilizare</h2>
            <p>
              Aplicația este simplu de utilizat, urmând pașii de mai jos:
            </p>
            <ol className="list-decimal ml-5">
              <li>Conectați-vă sau creați un cont nou</li>
              <li>Accesați secțiunea de cursuri pentru a vizualiza toate cursurile disponibile</li>
              <li>Parcurgeți lectile în propriul ritm</li>
              <li>Testați-va noile cunoștințe</li>
            </ol>
          </section>
          <section id="technologies" className="mb-10">
            <h2 className="text-2xl font-bold mb-4">Tehnologii utilizate</h2>
            <p>
              Pentru dezvoltarea InfoEasy am folosit următoarele tehnologii:
            </p>
            <h3 className="text-xl font-semibold mb-2">ReactJS</h3>
            <p>
            React este o bibliotecă JavaScript dezvoltată de Facebook, utilizată pentru construirea interfețelor de utilizator interactive și dinamice. Am ales să folosesc React pentru platforma educațională datorită eficienței sale în gestionarea componentelor reutilizabile și a stării aplicației, ceea ce ne permite să dezvoltăm o interfață rapidă și receptivă. React oferă un flux de date unidirecțional și utilizează un Virtual DOM pentru a optimiza actualizările UI, reducând astfel timpul de reacție și crescând performanța. Această bibliotecă este, de asemenea, susținută de o comunitate vastă și activă, oferind numeroase resurse și soluții pentru problemele comune, ceea ce ne-a permis să accelerăm procesul de dezvoltare și să implementăm cele mai bune practici în crearea unei platforme educaționale robuste și scalabile.
            </p>
            <h3 className="text-xl font-semibold mb-2">TailwindCSS</h3>
            <p>
            Am ales să folosesc Tailwind CSS în dezvoltarea aplicației datorită flexibilității și eficienței sale în crearea interfețelor utilizator. Tailwind CSS este un framework utilitar-first care permite stilizarea direct în markup, eliminând necesitatea de a scrie fișiere CSS personalizate extinse. Acest lucru a accelerat semnificativ procesul de dezvoltare, permițându-mi să creez rapid și eficient un design coerent și modern. Prin utilizarea claselor predefinite, am putut să experimentez și să ajusteze stilurile vizuale în mod iterativ, asigurând o experiență optimă pentru utilizatori. În plus, Tailwind CSS este extrem de personalizabil, ceea ce mi-a permis să adapteze tema și componentele vizuale pentru a se potrivi perfect cu viziunea și cerințele specifice ale aplicației.
            </p>
            <img src='../../src/assets/tailwindcss-ss.png'></img>
            <h3 className="text-xl font-semibold mb-2">HeadlessUI</h3>
            <p>
            Headless UI este o bibliotecă de componente UI care oferă funcționalități esențiale pentru interfața utilizatorului fără a impune un stil vizual specific. Am ales să folosesc Headless UI în cadrul platformei educaționale datorită flexibilității sale remarcabile, care permite integrarea și personalizarea completă a componentelor UI conform cerințelor și designului unic al platformei. Utilizând Headless UI, am reușit să creăm o interfață modernă și intuitivă, asigurând în același timp accesibilitate și ușurință în utilizare. Această abordare ne-a permis să ne concentrăm pe funcționalitatea și experiența utilizatorilor, fără a fi constrânși de limitările stilistice impuse de alte biblioteci UI.
            </p>
            <h3 className="text-xl font-semibold mb-2">GitLab</h3>
            <p>
            Am ales să folosesc GitLab pentru dezvoltarea aplicației datorită funcționalităților sale extinse de gestionare a codului sursă și integrării continue. GitLab oferă un set complet de instrumente pentru DevOps, inclusiv controlul versiunilor, CI/CD (integrare continuă și livrare continuă), și gestionarea problemelor, toate într-o platformă unificată. Aceste caracteristici mi-au permis să mențin un flux de lucru organizat și eficient, facilitând dezvoltarea rapidă și de înaltă calitate a aplicației. Instrumentele puternice de gestionare a proiectelor și monitorizare a performanței oferite de GitLab au fost esențiale pentru a mă asigura că dezvoltarea aplicației a fost realizată conform planificării și standardelor stabilite.
            </p><p>
Am ales GitLab în detrimentul GitHub și din motive etice, dat fiind angajamentul său ferm față de open-source și controlul utilizatorilor asupra datelor lor. GitLab este o platformă open-source, ceea ce îmi oferă transparență totală și flexibilitatea de a personaliza și extinde funcționalitățile sale în conformitate cu nevoile mele specifice. În plus, GitLab promovează și susține valorile comunității open-source, spre deosebire de GitHub, care este deținut de o corporație mare și a fost subiectul unor critici privind utilizarea și monetizarea datelor utilizatorilor. Alegând GitLab, am dorit să sprijin o platformă care respectă și promovează principiile etice și de colaborare deschisă, permițând utilizatorilor să contribuie la viitoarele versiuni ale aplicației. Aceasta deschide oportunitatea pentru comunitate de a se implica activ în dezvoltare, îmbunătățind aplicația și asigurându-se că răspunde nevoilor unei baze diverse de utilizatori.
            </p>
            <h3 className="text-xl font-semibold mb-2">Visual Studio Code</h3>
            <p>
            Am ales să folosesc Visual Studio Code (VSCode) în dezvoltarea aplicației datorită versatilității și eficienței pe care le oferă acest editor de cod. VSCode este un instrument puternic și personalizabil, care suportă o gamă largă de extensii ce pot îmbunătăți considerabil fluxul de lucru al dezvoltatorilor. Funcționalități precum IntelliSense, care oferă completare automată inteligentă și sugestii de cod, debugger-ul integrat, și suportul excelent pentru controlul versiunilor prin Git, au fost esențiale pentru a facilita un proces de dezvoltare rapid și eficient. 
            </p>
            <img src='../../src/assets/vscode-ss.png'></img>
            </section>
            <section id="system-requirements" className="mb-10">
            <h2 className="text-2xl font-bold mb-4">Cerințe de sistem</h2>
            <p>
            Pentru a folosi aplicația InfoEasy aveți nevoie doar de următoarele:
            </p>
            <ul className="list-disc ml-5">
            <li>Un dispozitiv cu conexiune la internet</li>
            <li>Dorința de a învață informatică și a descoperi un domeniu fascinant</li>
            </ul>
            </section>
            <section id="useful-links" className="mb-10">
            <h2 className="text-2xl font-bold mb-4">Linkuri utile</h2>
            <ul className="list-disc ml-5">
            <li><a href="https://gitlab.com/danielpos178/InfoEasy" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">Repository GitHub</a></li>
            <li><a href="https://gitlab.com/danielpos178/InfoEasy/-/blob/925d7a27f78f241cd92d5beb28baa1c72cbb33fe/README.md" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">README file</a></li>
            <li><a href="https://gitlab.com/danielpos178/InfoEasy/-/blob/925d7a27f78f241cd92d5beb28baa1c72cbb33fe/License.md" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">Licenta</a></li>
            <li><a href="mailto:danielpos178@gmail.com" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">Contactați-ne</a></li>
            </ul>
            </section>
            <section id="contribution" className="mb-10">
            <h2 className="text-2xl font-bold mb-4">Contribuție</h2>
            <p>
            Dacă doriți să contribuiți la dezvoltarea aplicației InfoEasy, vă rugăm să vizitați repository-ul nostru GitHub și să trimiteți un pull request cu modificările propuse.
            </p>
            <p>
            Pentru mai multe detalii privind contribuția, consultați <a href="https://github.com/username/infoeasy/blob/main/CONTRIBUTING.md" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">ghidul de contribuție</a>.
            </p>
            </section>
            </div>
            </main>
            </>
            );
            }

            export default Docs;