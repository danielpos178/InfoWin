export default function PE() {
    return (
      
        <section className="bg-white dark:bg-black min-h-screen flex">
          <div className="px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6  content-center">
            <div className="mx-auto max-w-screen-sm text-center">
              <h1 className="mb-4 text-7xl tracking-tight font-extrabold lg:text-9xl text-red-500 dark:text-red-400">404</h1>
              <p className="mb-4 text-3xl tracking-tight font-bold md:text-4xl text-blue-700 dark:text-blue-300">Something&apos;s missing.</p>
              <p className="mb-4 text-lg font-light text-blue-700 dark:text-blue-300">Sorry, we can&apos;t find that page. You&apos;ll find lots to explore on the home page. </p>
              <a href="#" className="inline-flex text-white bg-blue-600 hover:bg-blue-500 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:focus:ring-primary-900 my-4">Back to Homepage</a>
            </div>   
          </div>
        </section>
      
    );
  }
  