import Tabs from "../components/Tabs";

function Home() {
    return (
      <> 
        <div className="flex flex-wrap flex-col justify-center content-center h-screen w-full bg-white dark:bg-black">
          <h1 className="text-darkblue text-7xl font-black text-center pb-10 dark:text-blue-200">
            Info<span className="text-red-500 dark:text-red-400">Easy</span>
          </h1>
          <h1 className="dark:text-blue-200 text-darkblue text-2xl p-5 font-semibold">
            Invata informatica mai usor si in ritmul tau
          </h1>
        </div>
        <div className="w-full h-auto">
          <Tabs/>
        </div>
      </> 
    );
  }

export default Home
  