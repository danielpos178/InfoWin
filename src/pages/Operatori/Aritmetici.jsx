function Aritmetici() {
    return (
      <>
        <section className="text-darkblue dark:text-blue-300 body-font bg-white dark:bg-black min-h-screen pb-20">
          <section className="text-darkblue dark:text-blue-300 body-font md:pb-20">
            <div className="container px-5 py-24 mx-auto">
              <div className="flex flex-col text-center w-full mb-20 md:pb-10">
                <h2 className="text-xs tracking-widest font-medium title-font mb-1">Clasa a IX-a</h2>
                <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4">Operatori aritmetici</h1>
                <p className="lg:w-2/3 mx-auto leading-relaxed text-lg">
                  Operatorii aritmetici sunt utilizați pentru a calcula o valoare din două sau mai multe numere sau pentru a modifica semnul unui număr din pozitiv în negativ sau invers.
                </p>
              </div>
              <div className="flex flex-wrap lg:items-center lg:justify-center">
                <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Operatorii aritmetici unari</h2>
                  <p className="leading-relaxed mb-4">
                    <li className="list-none">
                      <b className="text-xl">+</b> returnează valoarea operandului
                    </li>
                    <li className="list-none">
                      <b className="text-xl">-</b> returnează valoarea operandului cu semn schimbat
                    </li>
                  </p>
                </div>
                <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Operatorii aritmetici binari</h2>
                  <p className="leading-relaxed mb-4">Semnificația acestor operatori este, în mare măsură, cunoscută de la matematică.</p>
                  <a className="text-white bg-blue-600 rounded-md p-1 inline-flex items-center" href="#binari">
                    Află mai multe
                    <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                      <path d="M5 12h14M12 5l7 7-7 7"></path>
                    </svg>
                  </a>
                </div>
                <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Testează-ți cunoștințele</h2>
                  <p className="leading-relaxed mb-4">
                    Completează următoarele întrebări pentru a-ți consolida informațiile noi învățate.
                  </p>
                  <a className="text-white bg-blue-600 rounded-md p-1 inline-flex items-center" href="/app/operatori/aritmetici/test">
                    Test
                    <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                      <path d="M5 12h14M12 5l7 7-7 7"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          </section>
          <section id="binari" className="flex text-center items-center justify-center text-lg">
            <div className="overflow-x-auto bg-white dark:bg-black md:w-2/3">
              <table className="min-w-full text-left text-md whitespace-nowrap">
                <caption className="pb-5 text-sm">Tabel operatori aritmetici binari</caption>
                <thead className="uppercase tracking-wider border-b-2 dark:border-blue">
                  <tr>
                    <th scope="col" className="px-6 py-5">Operator</th>
                    <th scope="col" className="px-6 py-5">Nume</th>
                    <th scope="col" className="px-6 py-5">Scop</th>
                    <th scope="col" className="px-6 py-5">Exemplu</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="border-b dark:border-blue hover:bg-blue-300 dark:hover:bg-blue-800">
                    <th scope="row" className="px-6 py-5"><b>+</b></th>
                    <td className="px-6 py-5">plus</td>
                    <td className="px-6 py-5">adunarea a două numere</td>
                    <td className="px-6 py-5">1+2 (=2)</td>
                  </tr>
                  <tr className="border-b dark:border-blue hover:bg-blue-300 dark:hover:bg-blue-800">
                    <th scope="row" className="px-6 py-5"><b>-</b></th>
                    <td className="px-6 py-5">minus</td>
                    <td className="px-6 py-5">diferența dintre două numere</td>
                    <td className="px-6 py-5">3-2 (=1)</td>
                  </tr>
                  <tr className="border-b dark:border-blue hover:bg-blue-300 dark:hover:bg-blue-800">
                    <th scope="row" className="px-6 py-5"><b>*</b></th>
                    <td className="px-6 py-5">ori</td>
                    <td className="px-6 py-5">înmulțirea a două numere</td>
                    <td className="px-6 py-5">2*1 (=2)</td>
                  </tr>
                  <tr className="border-b dark:border-blue hover:bg-blue-300 dark:hover:bg-blue-800">
                    <th scope="row" className="px-6 py-5"><b>/</b></th>
                    <td className="px-6 py-5">div</td>
                    <td className="px-6 py-5">câtul împărțirii a două numere</td>
                    <td className="px-6 py-5">15/5 (=3)</td>
                  </tr>
                  <tr className="border-b dark:border-blue hover:bg-blue-300 dark:hover:bg-blue-800">
                    <th scope="row" className="px-6 py-5"><b>%</b></th>
                    <td className="px-6 py-5">modulo</td>
                    <td className="px-6 py-5">restul împărțirii a două numere întregi</td>
                    <td className="px-6 py-5">7%2 (=1)</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </section>
        </section>
      </>
    );
  }
  
  export default Aritmetici;