function Logici() {
    return (
      <>
        <section className="text-darkblue dark:text-blue-300 dark:bg-black body-font bg-white min-h-screen pb-20">
          <section className="text-darkblue dark:text-blue-300 body-font md:pb-20">
            <div className="container px-5 py-24 mx-auto">
              <div className="flex flex-col text-center w-full mb-20 md:pb-10">
                <h2 className="text-xs tracking-widest font-medium title-font mb-1">Clasa a IX-a</h2>
                <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4">Operatori logici</h1>
                <p className="lg:w-2/3 mx-auto leading-relaxed text-lg">Operatorii logici sunt folosiți pentru a combina sau evalua condiții multiple într-un program. Aceștia permit programatorilor să creeze expresii complexe de control al fluxului în codul lor.</p>
              </div>
              <div className="flex flex-wrap lg:items-center lg:justify-center pb-10">
                <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Operatorii logici în pseudocod</h2>
                  <p className="leading-relaxed mb-4">
                    <li className="list-none">În pseudocod, operatorii logici sunt folosiți pentru a combina sau evalua condiții multiple.</li>
                  </p>
                  <p><b>De exemplu:</b>
                    <pre><code> dacă (condiție1 ȘI condiție2) atunci </code></pre>
                  </p>
                </div>
                <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Operatorii logici în JavaScript</h2>
                  <p className="leading-relaxed mb-4">În JavaScript, operatorii logici sunt similari cu cei din pseudocod.</p>
                  <p><b>De exemplu:</b>
                    <pre><code> if (condiție1 && condiție2) </code></pre>
                  </p>
                </div>
                <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Testează-ți cunoștințele</h2>
                  <p className="leading-relaxed mb-4">Completează următoarele întrebări pentru a-ți verifica cunoștințele dobândite despre operatorii logici.</p>
                  <a className="text-white bg-blue-600 rounded-md p-1 inline-flex items-center" href="/app/operatori/logici/test">Test
                    <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                      <path d="M5 12h14M12 5l7 7-7 7"></path>
                    </svg>
                  </a>
                </div>
              </div>
              <table className="min-w-full text-left text-md whitespace-nowrap mt-16">
                      <caption className="pb-5 text-sm">Tabel operatori logici</caption>
                      <thead className="uppercase tracking-wider border-b-2 dark:border-blue">
                        <tr>
                          <th scope="col" className="px-6 py-5">Operator</th>
                          <th scope="col" className="px-6 py-5">Semnificație</th>
                          <th scope="col" className="px-6 py-5">Exemplu (pseudocod)</th>
                          <th scope="col" className="px-6 py-5">Exemplu (C++)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr className="border-b dark:border-blue hover:bg-blue-300 dark:hover:bg-blue-800">
                          <th scope="row" className="px-6 py-5"><b>ȘI</b></th>
                          <td className="px-6 py-5">Și</td>
                          <td className="px-6 py-5">dacă (condiție1 ȘI condiție2) atunci</td>
                          <td className="px-6 py-5">if (condiție1 && condiție2)</td>
                        </tr>
                        <tr className="border-b dark:border-blue hover:bg-blue-300 dark:hover:bg-blue-800">
                          <th scope="row" className="px-6 py-5"><b>SAU</b></th>
                          <td className="px-6 py-5">Sau</td>
                          <td className="px-6 py-5">dacă (condiție1 SAU condiție2) atunci</td>
                          <td className="px-6 py-5">if (condiție1 || condiție2)</td>
                        </tr>
                        <tr className="border-b dark:border-blue hover:bg-blue-300 dark:hover:bg-blue-800">
                          <th scope="row" className="px-6 py-5"><b>NU</b></th>
                          <td className="px-6 py-5">Nu</td>
                          <td className="px-6 py-5">dacă (NU condiție1) atunci</td>
                          <td className="px-6 py-5">if (!condiție1)</td>
                        </tr>
                      </tbody>
                    </table>
            </div>
          </section>
        </section>    
      </>
    );
  }
  
  export default Logici;