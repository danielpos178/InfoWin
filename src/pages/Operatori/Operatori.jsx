export default function Operatori() {
  return (
    <>
      <section className="text-darkblue body-font dark:bg-black dark:text-blue-200 body-font bg-white min-h-screen">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-wrap w-full flex-row-reverse mb-20">
            <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
              <h1 className="sm:text-sm text-sm md:text-md font-medium title-font mb-2 md:text-right sm:text-center text-center">Clasa 9</h1>
              <h1 className="sm:text-3xl text-3xl md:text-4xl font-semibold title-font mb-2 md:text-right sm:text-center text-center">Operatori</h1>
            </div>
            <p className="lg:w-1/2 w-full leading-relaxed text-darkblue dark:text-blue-300 pt-5">O operație este alcătuită din operanzi și operator. Operanzii reprezintă datele cu care se fac operațiile, iar operatorul este simbolul care stabilește ce operație se face cu operanzii.</p>
          </div>
          <div className="flex flex-wrap -m-4 mt-18">
            <div className="xl:w-1/4 md:w-1/2 w-screen">
              <div className="p-6 rounded-lg">
                <div className="bg-gradient-to-tl from-blue-600 to-blue-500 h-40 text-white text-3xl flex items-center justify-center flex-wrap rounded-lg w-full"><a href='/app/operatori/aritmetici'>Operatori aritmetici</a></div>
              </div>
            </div>
            <div className="xl:w-1/4 md:w-1/2 w-screen">
              <div className="bg-gray-100 p-6 rounded-lg">
                <div className="bg-gradient-to-tl from-blue-600 to-blue-500 h-40 text-white text-3xl flex items-center justify-center flex-wrap rounded-lg"><a href='/app/operatori/atribuire'>Operatori de atribuire</a></div>
              </div>
            </div>
            <div className="xl:w-1/4 md:w-1/2 w-screen">
              <div className="bg-gray-100 p-6 rounded-lg">
                <div className="bg-gradient-to-tl from-blue-600 to-blue-500 h-40 text-white text-3xl flex items-center justify-center flex-wrap rounded-lg"><a href='/app/operatori/relationali'>Operatori relaționali</a></div>
              </div>
            </div>
            <div className="xl:w-1/4 md:w-1/2 w-screen">
              <div className="bg-gray-100 p-6 rounded-lg">
                <div className="bg-gradient-to-tl from-blue-600 to-blue-500 h-40 text-white text-3xl flex items-center justify-center flex-wrap rounded-lg"><a href='/app/operatori/logici'>Operatori logici</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
