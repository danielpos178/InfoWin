function Relationali() {
    return (
      <>
        <section className="text-darkblue dark:text-blue-300 dark:bg-black body-font bg-white min-h-screen pb-20">
          <section className="text-darkblue dark:text-blue-300 body-font md:pb-20">
            <div className="container px-5 py-24 mx-auto">
              <div className="flex flex-col text-center w-full mb-20 md:pb-10">
                <h2 className="text-xs tracking-widest font-medium title-font mb-1">Clasa a IX-a</h2>
                <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4">Operatori relaționali</h1>
                <p className="lg:w-2/3 mx-auto leading-relaxed text-lg">Operatorii relaționali sunt folosiți pentru a compara două valori. Aceste comparații sunt esențiale pentru luarea deciziilor în cadrul unui program, permițând execuția condiționată a codului.</p>
              </div>
              <div className="flex flex-wrap lg:items-center lg:justify-center">
                <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Operatorii relaționali în pseudocod</h2>
                  <p className="leading-relaxed mb-4">
                    <li className="list-none">În pseudocod, operatorii relaționali sunt folosiți pentru a compara două valori. Acești operatori sunt esențiali pentru a controla fluxul execuției programului.</li>
                  </p>
                  <p><b>De exemplu:</b>
                    <pre><code> dacă (valoare1 &gt; valoare2) atunci </code></pre>
                  </p>
                </div>
                <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Operatorii relaționali în C++</h2>
                  <p className="leading-relaxed mb-4">În limbajul de programare C++, operatorii relaționali sunt similari cu cei din pseudocod.</p>
                  <p><b>De exemplu:</b>
                    <pre><code> if (valoare1 &gt; valoare2) </code></pre>
                  </p>
                </div>
                <div className="xl:w-1/4 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Testează-ți cunoștințele</h2>
                  <p className="leading-relaxed mb-4">Completează următoarele întrebări pentru a îți consolida informațiile noi învățate.</p>
                  <a className="text-white bg-blue-600 rounded-md p-1 inline-flex items-center" href="/app/operatori/relationali/test">Test
                    <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                      <path d="M5 12h14M12 5l7 7-7 7"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          </section>
        </section>    
      </>
    );
  }
  
  export default Relationali;
  