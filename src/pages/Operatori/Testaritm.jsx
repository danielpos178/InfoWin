import React, { useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';


const questions = [
  {
    id: 1,
    question: "Care este simbolul pentru operația de adunare?",
    options: ['+', '-', '*', '/'],
    correctAnswer: '+',
  },
  {
    id: 2,
    question: 'Care este simbolul pentru operația de scădere?',
    options: ['\\', '%', '-', '--'],
    correctAnswer: '-',
  },
  {
    id: 3,
    question: 'Care este raspunsul pentru: 3*5?',
    options: ['8', '0', '2', '15'],
    correctAnswer: '15',
  },
  {
    id: 4,
    question: 'Cum se numește operatorul "/" ?',
    options: ['slash', 'backslash', 'div', 'mod'],
    correctAnswer: 'div',
  },
  {
    id: 5,
    question: 'Care este rezultatul pentru 6/3',
    options: ['8', '0', '2', '15'],
    correctAnswer: '2',
  },
  {
    id: 6,
    question: 'Cum se numește operatorul "%" ?',
    options: ['slash', 'backslash', 'div', 'mod'],
    correctAnswer: 'mod',
  },
];

function Quiz() {
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState('');
  const [showResult, setShowResult] = useState(false);
  const [score, setScore] = useState(0);

  const handleAnswerSelect = (option) => {
    setSelectedAnswer(option);
  };

  const handleNextQuestion = () => {
    if (selectedAnswer === questions[currentQuestion].correctAnswer) {
      setScore(score + 1);
    }

    if (currentQuestion + 1 < questions.length) {
      setSelectedAnswer('');
      setCurrentQuestion(currentQuestion + 1);
    } else {
      setShowResult(true);
    }
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-900 text-gray-200 dark:bg-gray-900 dark:text-white">
    <div className="max-w-md w-full bg-gray-100 dark:bg-gray-800 p-8 rounded-md shadow-md">
      <h1 className="text-xl font-bold mb-4">{questions[currentQuestion].question}</h1>
      <div className="space-y-4">
        {questions[currentQuestion].options.map((option, index) => (
          <button
            key={index}
            className={`w-full p-3 rounded-md border ${
              selectedAnswer === option
                ? 'bg-blue-500 dark:bg-blue-600 text-white'
                : 'bg-gray-200 dark:bg-gray-700 text-gray-800 dark:text-white'
            }`}
            onClick={() => handleAnswerSelect(option)}
          >
            {option}
          </button>
        ))}
      </div>
      <button
        onClick={handleNextQuestion}
        className="w-full mt-4 px-4 py-2 rounded-md bg-blue-500 dark:bg-blue-600 text-white font-bold"
      >
        {currentQuestion === questions.length - 1 ? 'Ai terminat' : 'Urmatoarea intrebare'}
      </button>
      <Transition show={showResult} as={React.Fragment}>
        <Dialog onClose={() => setShowResult(false)}>
          <Dialog.Overlay className="fixed inset-0 bg-black opacity-50" />
          <Transition.Child
            as={React.Fragment}
            enter="transition ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 flex items-center justify-center">
              <div className="bg-white dark:bg-gray-700 p-8 rounded-md shadow-md">
                <Dialog.Title className="text-lg font-bold">
                  
                </Dialog.Title>
                <div className="flex items-center justify-center mt-4">
                  <p className="mr-2">Ai obtinut</p>
                  <p className="text-xl font-bold">{score}/{questions.length}</p>
                </div>
                <div className="mt-4 flex justify-center">
                  <button
                    onClick={() => {
                      setShowResult(false);
                      setCurrentQuestion(0);
                      setSelectedAnswer('');
                      setScore(0);
                    }}
                    className="px-4 py-2 bg-blue-500 dark:bg-blue-600 text-white rounded-md"
                  >
                    Reseteaza
                  </button>
                </div>
              </div>
            </div>
          </Transition.Child>
        </Dialog>
      </Transition>
    </div>
  </div>
  );
}

export default Quiz;