import React, { useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';


const questions = [
  {
    id: 1,
    question: "Ce face un operator relațional în pseudocod și C++?",
    options: ['Atribuie o valoare unei variabile', 'Compară două valori', 'Realizează o operație matematică'],
    correctAnswer: 'Compară două valori',
  },
  {
    id: 2,
    question: 'Care este sintaxa corectă pentru un operator relațional în C++?',
    options: ['variabila = valoare', 'variabila == valoare', 'variabila := valoare'],
    correctAnswer: 'variabila == valoare',
  },
  {
    id: 3,
    question: 'Care dintre următoarele este un exemplu corect de utilizare a unui operator relațional în pseudocod?',
    options: ['dacă variabila == 10 atunci', '10 = variabila', 'variabila -> 10'],
    correctAnswer: 'dacă variabila == 10 atunci',
  },
  {
    id: 4,
    question: 'Ce rol are operatorul relațional <=?',
    options: ['Verifică dacă o variabilă este mai mare decât o valoare', 'Verifică dacă o variabilă este mai mică sau egală cu o valoare', 'Verifică dacă o variabilă este diferită de o valoare', 'Acest operator nu exista'],
    correctAnswer: 'Verifică dacă o variabilă este mai mică sau egală cu o valoare',
  },
  {
    id: 5,
    question: 'Ce efect are utilizarea operatorului relațional != asupra valorii unei variabile?',
    options: ['Compară valoarea variabilei cu o altă valoare', 'Atribuie o valoare variabilei', 'Nu are niciun efect asupra valorii variabilei'],
    correctAnswer: 'Compară valoarea variabilei cu o altă valoare',
  },
];

function Quizaritm() {
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState('');
  const [showResult, setShowResult] = useState(false);
  const [score, setScore] = useState(0);

  const handleAnswerSelect = (option) => {
    setSelectedAnswer(option);
  };

  const handleNextQuestion = () => {
    if (selectedAnswer === questions[currentQuestion].correctAnswer) {
      setScore(score + 1);
    }

    if (currentQuestion + 1 < questions.length) {
      setSelectedAnswer('');
      setCurrentQuestion(currentQuestion + 1);
    } else {
      setShowResult(true);
    }
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-900 text-gray-200 dark:bg-gray-900 dark:text-white">
    <div className="max-w-md w-full bg-gray-100 dark:bg-gray-800 p-8 rounded-md shadow-md">
      <h1 className="text-xl font-bold mb-4">{questions[currentQuestion].question}</h1>
      <div className="space-y-4">
        {questions[currentQuestion].options.map((option, index) => (
          <button
            key={index}
            className={`w-full p-3 rounded-md border ${
              selectedAnswer === option
                ? 'bg-blue-500 dark:bg-blue-600 text-white'
                : 'bg-gray-200 dark:bg-gray-700 text-gray-800 dark:text-white'
            }`}
            onClick={() => handleAnswerSelect(option)}
          >
            {option}
          </button>
        ))}
      </div>
      <button
        onClick={handleNextQuestion}
        className="w-full mt-4 px-4 py-2 rounded-md bg-blue-500 dark:bg-blue-600 text-white font-bold"
      >
        {currentQuestion === questions.length - 1 ? 'Ai terminat' : 'Urmatoarea intrebare'}
      </button>
      <Transition show={showResult} as={React.Fragment}>
        <Dialog onClose={() => setShowResult(false)}>
          <Dialog.Overlay className="fixed inset-0 bg-black opacity-50" />
          <Transition.Child
            as={React.Fragment}
            enter="transition ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 flex items-center justify-center">
              <div className="bg-white dark:bg-gray-700 p-8 rounded-md shadow-md">
                <Dialog.Title className="text-lg font-bold">
                  
                </Dialog.Title>
                <div className="flex items-center justify-center mt-4">
                  <p className="mr-2">Ai obtinut</p>
                  <p className="text-xl font-bold">{score}/{questions.length}</p>
                </div>
                <div className="mt-4 flex justify-center">
                  <button
                    onClick={() => {
                      setShowResult(false);
                      setCurrentQuestion(0);
                      setSelectedAnswer('');
                      setScore(0);
                    }}
                    className="px-4 py-2 bg-blue-500 dark:bg-blue-600 text-white rounded-md"
                  >
                    Reseteaza
                  </button>
                </div>
              </div>
            </div>
          </Transition.Child>
        </Dialog>
      </Transition>
    </div>
  </div>
  );
}

export default Quizaritm;