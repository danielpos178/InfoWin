function Pseudocod() {
    return (
      <>
        <section className="text-darkblue dark:text-blue-300 dark:bg-black body-font bg-white min-h-screen pb-20">
          <section className="text-darkblue dark:text-blue-300 body-font md:pb-20">
            <div className="container px-5 py-24 mx-auto">
              <div className="flex flex-col text-center w-full mb-20 md:pb-10">
                <h2 className="text-xs tracking-widest font-medium title-font mb-1">Introducere</h2>
                <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4">Pseudocod: Ce trebuie să știi</h1>
                <p className="lg:w-2/3 mx-auto leading-relaxed text-lg">Pseudocodul este o modalitate de a descrie algoritmi folosind un limbaj simplu și ușor de înțeles. Este folosit pentru a planifica și a documenta logicile de programare înainte de a le implementa într-un limbaj de programare specific.</p>
              </div>
              <div className="flex flex-wrap lg:items-center lg:justify-center">
                <div className="xl:w-1/2 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Ce este pseudocodul?</h2>
                  <p className="leading-relaxed mb-4">
                    Pseudocodul este un limbaj de programare informal care permite unui programator să descrie algoritmii într-un mod clar și concis, fără a se concentra pe sintaxa specifică a unui anumit limbaj de programare.
                  </p>
                </div>
                <div className="xl:w-1/2 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Cum se folosește pseudocodul?</h2>
                  <p className="leading-relaxed mb-4">Pseudocodul este folosit pentru a planifica și a documenta algoritmii. De obicei, este utilizat în fazele inițiale ale dezvoltării software pentru a clarifica și a comunica modul în care va funcționa un algoritm.</p>
                </div>
              </div>
              <div className="flex flex-wrap lg:items-center lg:justify-center mt-12">
                <div className="xl:w-1/2 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Exemplu de pseudocod</h2>
                  <p className="leading-relaxed mb-4">
                    Iată un exemplu simplu de pseudocod care calculează suma a două numere:
                    <pre><code>
                      început<br />
                      &nbsp;&nbsp;citeste numar1<br />
                      &nbsp;&nbsp;citeste numar2<br />
                      &nbsp;&nbsp;suma = numar1 + numar2<br />
                      &nbsp;&nbsp;afișează suma<br />
                      sfârșit
                    </code></pre>
                  </p>
                </div>
                <div className="xl:w-1/2 lg:w-1/2 md:w-full px-8 py-6 border-l-2 border-gray-200 border-opacity-60">
                  <h2 className="md:text-xl sm:text-xl font-medium title-font mb-2">Testează-ți cunoștințele</h2>
                  <p className="leading-relaxed mb-4">Completează următoarele întrebări pentru a-ți consolida informațiile noi învățate despre pseudocod.</p>
                  <a className="text-white bg-blue-600 rounded-md p-1 inline-flex items-center" href="/app/pseudocod/test">Test
                    <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                      <path d="M5 12h14M12 5l7 7-7 7"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          </section>
        </section>
      </>
    );
  }
  
  export default Pseudocod;