export default function S14() {
    return(
        <>
            <div
                className="flex flex-wrap flex-col justify-center content-center w-full bg-white dark:bg-black pt-10 sm:pt-16">
                <h1 className="dark:text-blue-200 text-darkblue text-3xl sm:text-4xl pr-9 p-5">Subiecte Bacalaureat 2014</h1>
            </div>
            <section className="px-14 pb-28 bg-white dark:bg-black">
                <div
                    className="flex flex-wrap flex-col justify-center content-center w-full bg-white dark:bg-black pt-10 sm:pt-16 dark:text-blue-200 text-darkblue">
                    <ul className="list-none">
                        <li className="text-2xl pb-10">
                            <ul className="list-disc">
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/main/src/pages/SubiecteBac/14/E_d_Informatica_C_sp_MI_2014_var_04_LRO.pdf?ref_type=heads&inline=false">Subiect sesiunea iunie-iulie</a><br/>
                                </li>
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/main/src/pages/SubiecteBac/14/E_d_Informatica_sp_MI_2014_bar_04_LRO.pdf?ref_type=heads&inline=false">Barem sesiunea iunie-iulie</a>
                                </li>
                            </ul>
                        </li>
                        <li className="text-2xl pb-10">
                            <ul className="list-disc">
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/main/src/pages/SubiecteBac/14/E_d_Informatica_C_sp_MI_2014_var_10_LRO.pdf?ref_type=heads&inline=false">Subiect sesiunea august</a><br/>
                                </li>
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/main/src/pages/SubiecteBac/14/E_d_Informatica_sp_MI_2014_bar_10_LRO.pdf?ref_type=heads&inline=false">Barem sesiunea august</a>
                                </li>
                            </ul>
                        </li>
                        <li className="text-2xl pb-10">
                            <ul className="list-disc">
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/main/src/pages/SubiecteBac/14/E_d_Informatica_C_sp_MI_2014_var_02_LRO.pdf?ref_type=heads&inline=false">Subiect sesiunea speciala</a><br/>
                                </li>
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/main/src/pages/SubiecteBac/14/E_d_Informatica_sp_MI_2014_bar_02_LRO.pdf?ref_type=heads&inline=false">Barem sesiunea speciala</a>
                                </li>
                            </ul>
                        </li>


                    </ul>
                </div>
            </section>
        </>
    );
}
