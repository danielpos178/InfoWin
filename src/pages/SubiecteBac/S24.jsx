export default function S24() {
    return(
        <>
            <div className="flex flex-wrap flex-col justify-center content-center w-full bg-white dark:bg-black pt-10 sm:pt-16">
                <h1 className="dark:text-blue-200 text-darkblue text-3xl sm:text-4xl pr-9 p-5">Subiecte Bacalaureat 2024</h1>
            </div>
            <section className="px-14 pb-28 bg-white dark:bg-black">
                <div className="flex flex-wrap flex-col justify-center content-center w-full bg-white dark:bg-black pt-10 sm:pt-16 dark:text-blue-200 text-darkblue">
                    <ul className="list-none">
                        <li className="text-2xl pb-10">
                            <ul className="list-disc">
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/3b7317886bceeeaa9ed425fd293a43445612c32f/src/pages/SubiecteBac/24/E_d_Informatica_2024_MI_C_var_simulare_LRO.pdf?inline=false">Simulare (6 martie 2024)</a><br/>
                                </li>
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/3b7317886bceeeaa9ed425fd293a43445612c32f/src/pages/SubiecteBac/24/E_d_Informatica_2024_MI_bar_simulare_LRO.pdf?inline=false">Barem simulare(6 martie 2024)</a>
                                </li>
                            </ul>
                        </li>
                        <li className="text-2xl">
                            <ul className="list-disc">
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/3b7317886bceeeaa9ed425fd293a43445612c32f/src/pages/SubiecteBac/24/E_D_Informatica_2024_sp_MI_C_var_model.pdf?inline=false" >Model de subiect (noiembrie 2023)</a><br/>
                                </li>
                                <li>
                                    <a href="https://gitlab.com/danielpos178/InfoEasy/-/raw/3b7317886bceeeaa9ed425fd293a43445612c32f/src/pages/SubiecteBac/24/E_D_Informatica_2024_sp_MI_bar_model.pdf?inline=false">Barem model (noiembrie 2023)</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </section>
        </>
    );
}
