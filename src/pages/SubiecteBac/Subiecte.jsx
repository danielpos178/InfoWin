export default function Subiecte() {
    return(
        <>
            <div
                className="flex flex-col text-center w-full mb-20 md:pb-10 dark:text-blue-200 text-darkblue pt-10 sm:pt-16">
                <h1 className="sm:text-5xl text-3xl title-font mb-1 font-bold   ">Subiecte Bacalaureat</h1>
                <h2 className="text-md tracking-widest font-medium title-font mb-4">-subiecte, bareme, modele-</h2>
                <p className="lg:w-2/3 mx-auto leading-relaxed text-lg">Subiectele si baremele au fost preluate de pe website-urile <a href="https://rocnee.eu/" target="_blank" className="underline">rocnee.eu</a> și <a href="http://subiecte.edu.ro" target="_blank" className="underline">subiecte.edu.ro</a></p>
            </div>
            <section
                className="text-darkblue dark:text-blue-300 body-font body-font bg-white dark:bg-black min-h-screen pb-20 pt-6 sm:pt-14">
                <div className="container px-5 py-24 mx-auto">
                    <div className="flex flex-wrap -m-4">
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s24'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s24'>2024</a>
                                </div>
                            </a>

                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s23'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s23'>2023</a>
                                </div>
                            </a>

                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s22'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s22'>2022</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s21'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s21'>2021</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s20'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s20'>2020</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s19'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s19'>2019</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s18'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s18'>2018</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s17'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s17'>2017</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s16'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s22'>2016</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s15'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s21'>2015</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s14'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s20'>2014</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s13'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s19'>2013</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s12'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s18'>2012</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s11'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s17'>2011</a>
                                </div>
                            </a>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                            <a className="block relative h-48 rounded overflow-hidden" href='/app/bac/s10'>
                                <div
                                    className="bg-gradient-to-tl from-blue-600 to-blue-500 w-full h-full text-white text-3xl flex items-center justify-center flex-wrap">
                                    <a href='/app/bac/s17'>2010</a>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}
