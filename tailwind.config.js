/** @type {import('tailwindcss').Config} */


export default {
  darkMode: "class",
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      'black': '#0f0f0f',
      'white': '#f5f5f5',
      'red': {
          100: "#e4cccc",
          200: "#c99999",
          300: "#ae6666",
          400: "#933333",
          500: "#780000",
          600: "#600000",
          700: "#480000",
          800: "#300000",
          900: "#180000"
      },
      'darkred': '#2B0000',
      'darkblue': '#003049',
      'blue': {
          100: "#d6e0e6",
          200: "#adc2cd",
          300: "#85a3b5",
          400: "#5c859c",
          500: "#336683",
          600: "#295269",
          700: "#1f3d4f",
          800: "#142934",
          900: "#0a141a"
        },
    extend: {},
  },
  plugins: {
    'postcss-import': {},
     'autoprefixer': {},
     'tailwindcss': {},
     'flowbite/plugin' : {},

 }
}
}
